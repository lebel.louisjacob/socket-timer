import socket, time

if __name__ == '__main__':
    def algorithm():
        for x in range(1000000):
            print(x)

    with socket.socket() as client_socket:
        client_socket.connect(('', 5000))

        client_socket.send('time'.encode())
        time.sleep(1)
        client_socket.send('time'.encode())
        time.sleep(1)
        client_socket.send('print'.encode())
        time.sleep(1)
        client_socket.send('time'.encode())
        time.sleep(1)
        client_socket.send('print'.encode())
        client_socket.close()