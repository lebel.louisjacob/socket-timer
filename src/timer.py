import argparse, socket, time

class Commander:
    BUFFER_SIZE = 2048

    def __init__(self, port, commands={}):
        self.close = lambda: None
        self.__read = lambda: input()
        if port is not None:
            self.__server_socket = socket.socket()
            self.__server_socket.bind(('', port))
            self.__server_socket.listen(1)
            self.__client_socket = self.__server_socket.accept()[0]

            self.close = self.__close_sockets
            self.__read = lambda: self.__client_socket.recv(Commander.BUFFER_SIZE).decode()

        self.__is_running = False
        self.__commands = commands
        self.__commands['exit'] = self.stop
        self.__commands[''] = self.stop

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()

    def __close_sockets(self):
        self.__client_socket.close()
        self.__server_socket.close()

    def __call__(self):
        self.__is_running = True
        while self.__is_running:
            self.__commands[self.__read()]()

    def stop(self):
        self.__is_running = False

class Timer:
    def __init__(self):
        self.__time = time.time()
        self.__last = self.__time

    def time(self):
        self.__last = time.time() - self.__time
        self.__time = time.time()
        
    def stdout(self):
        print(self.__last)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='a socket timer to test algorithm performance')
    parser.add_argument('port', type=int, nargs='?', default=None, help='the port the timer will bind to. If not specified, the timer will bind to stdin')
    args = parser.parse_args()

    timer = Timer()

    with Commander(args.port, {'time': timer.time, 'print': timer.stdout}) as commander:
        commander()